import numpy as np
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable


df1, df2 = pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user1.features_labels.csv"), pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user2.features_labels.csv")

print(df1.shape)
print(df2.shape)

print(df1.info(verbose=True))
print(df2.info(verbose=True))

print(len(df1.columns))
print(len(df2.columns))

ft_labels_1, ft_labels_2 = [col for col in df1 if not col.startswith('label') and not col.startswith('timestamp')], [col for col in df2 if not col.startswith('label') and not col.startswith('timestamp')]

print(len(ft_labels_1), len(ft_labels_2))

seconds_in_day = float(60*60*24)
timestamps1, timestamps2 = df1['timestamp'], df2['timestamp'] 

for i in ft_labels_1:
    days_since_participation = (timestamps1 - timestamps1[0]) / float(seconds_in_day)

    feat_values = df1[i]
        
    fig = plt.figure(figsize=(10,3),facecolor='white')
        
    ax1 = plt.subplot(1,2,1)
    ax1.plot(days_since_participation,feat_values,'.-',markersize=3,linewidth=0.1)
    plt.xlabel('days of participation')
    plt.ylabel('feature value')
    plt.title('{}\nfunction of time'.format(i))
    plt.savefig("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/features/u1/user1_{}.png".format(i))

    
for i in ft_labels_1:
    days_since_participation = (timestamps2 - timestamps2[0]) / float(seconds_in_day)

    feat_values = df2[i]
        
    fig = plt.figure(figsize=(10,3),facecolor='white')
        
    ax1 = plt.subplot(1,2,1)
    ax1.plot(days_since_participation,feat_values,'.-',markersize=3,linewidth=0.1)
    plt.xlabel('days of participation')
    plt.ylabel('feature value')
    plt.title('{}\nfunction of time'.format(i))
    plt.savefig("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/features/u2/user2_{}.png".format(i))
    
