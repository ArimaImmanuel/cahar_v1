import numpy as np
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable


df1, df2 = pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user1.features_labels.csv"), pd.read_csv("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user2.features_labels.csv")

print(df1.shape)
print(df2.shape)

print(df1.info(verbose=True))
print(df2.info(verbose=True))

print(len(df1.columns))
print(len(df2.columns))

context_labels_1, context_labels_2 = [col for col in df1 if col.startswith('label:')], [col for col in df2 if col.startswith('label:')]

print(len(context_labels_1), len(context_labels_2))

labels, sizes = []*0, []*0

for i in context_labels_1:
    print("{} : {}".format(i[6:], df1[i].sum(axis = 0, skipna = True)))
    
    labels.append(i[6:]+':'+str(df1[i].sum(axis = 0, skipna = True)))
    sizes.append(df1[i].sum(axis = 0, skipna = True))

plt.figure(figsize=(100,100),facecolor='white')

plt.pie(sizes, labels=['' for _ in range(len(labels))])
plt.legend(labels, loc="upper right", prop={'size': 60}, bbox_to_anchor=(0.1,1))
plt.savefig("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user1.png")
#plt.show()

for i in context_labels_1:
    print("{} : {}".format(i[6:], df1[i].sum(axis = 0, skipna = True)))
    
    labels.append(i[6:]+':'+str(df1[i].sum(axis = 0, skipna = True)))
    sizes.append(df1[i].sum(axis = 0, skipna = True))

plt.figure(figsize=(100,100),facecolor='white')

plt.pie(sizes, labels=['' for _ in range(len(labels))])
plt.legend(labels, loc="upper right", prop={'size': 60}, bbox_to_anchor=(0.1,1))
plt.savefig("/mnt/sda4/Arima/PROJECTS/Outbox/YetToBePaid/CAHAR/DS/archive/user2.png")
#plt.show()
